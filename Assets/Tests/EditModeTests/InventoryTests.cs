﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class InventoryTests
    {
        Inventory inv;
        ItemInvEntry testItemEntry;

        [SetUp]
        public void Setup()
        {
            inv = new Inventory();
            testItemEntry = new ItemInvEntry();
        }

        [Test]
        public void EmptyInvTest()
        {
            Assert.That(inv != null);
        }

        [Test]
        public void InitTests()
        {
            bool closable = false;
            bool movable = true;
            Vector2Int size = new Vector2Int(8, 8);

            Assert.AreEqual(true, inv.Init(size, closable, movable), "Init returned a wrong value!");
            Assert.That(inv != null, "inv is a null");
            Assert.That(inv.inventorySize == size, "wrong size after Init");
            Assert.That(inv.closable == closable, "wrong closable after Init");
            Assert.That(inv.movable == movable, "wrong movable after Init");

            closable = true;
            movable = false;
            size = new Vector2Int(2, 4);

            Assert.AreEqual(true, inv.Init(size, closable, movable), "Init returned a wrong value!");
            Assert.That(inv != null, "inv is a null");
            Assert.That(inv.inventorySize == size, "wrong size after Init");
            Assert.That(inv.closable == closable, "wrong closable after Init");
            Assert.That(inv.movable == movable, "wrong movable after Init");

            size = new Vector2Int(1, 1);

            Assert.AreEqual(true, inv.Init(size, closable, movable), "Init returned a wrong value!");
            Assert.That(inv != null, "inv is a null");
            Assert.That(inv.inventorySize == size, "wrong size after Init");
            Assert.That(inv.closable == closable, "wrong closable after Init");
            Assert.That(inv.movable == movable, "wrong movable after Init");

            size = new Vector2Int(128, 256);

            Assert.AreEqual(true, inv.Init(size, closable, movable), "Init returned a wrong value!");
            Assert.That(inv != null, "inv is a null");
            Assert.That(inv.inventorySize == size, "wrong size after Init");
            Assert.That(inv.closable == closable, "wrong closable after Init");
            Assert.That(inv.movable == movable, "wrong movable after Init");

            size = new Vector2Int(int.MaxValue, 256);

            Assert.AreEqual(false, inv.Init(size, closable, movable), "Init returned a wrong value!");

            size = new Vector2Int(56, 257);

            Assert.AreEqual(false, inv.Init(size, closable, movable), "Init returned a wrong value!");

            size = new Vector2Int(0, 67);

            Assert.AreEqual(false, inv.Init(size, closable, movable), "Init returned a wrong value!");

            size = new Vector2Int(-1, 256);

            Assert.AreEqual(false, inv.Init(size, closable, movable), "Init returned a wrong value!");

            size = new Vector2Int(-1, 0);

            Assert.AreEqual(false, inv.Init(size, closable, movable), "Init returned a wrong value!");

            size = new Vector2Int(int.MinValue, 1);

            Assert.AreEqual(false, inv.Init(size, closable, movable), "Init returned a wrong value!");
        }

        [Test]
        public void AddRemoveTests()
        {
            inv.Init(new Vector2Int(6, 6));

            inv.AddItem(testItemEntry);

            Assert.AreEqual(1, inv.items.Count, "item Count has a wrong value!");

            inv.RemoveItem(testItemEntry);

            Assert.AreEqual(0, inv.items.Count, "item Count has a wrong value!");
        }

        [Test]
        public void SerializeTests()
        {
            inv.Init(new Vector2Int(6, 6));

            inv.AddItem(testItemEntry);

            InventorySerializableData data = inv.SerializeInventory();

            Assert.AreEqual(1, data.items.Count, "item Count has a wrong value!");
            Assert.AreEqual(testItemEntry, inv.items[0], "item entries are not the same");
            Assert.AreEqual(data.items[0], inv.items[0], "item entries are not the same");

            inv.Init(new Vector2Int(6, 6));

            Assert.AreEqual(0, inv.items.Count, "item Count has a wrong value!");

            inv.SetInventory(data);

            Assert.AreEqual(1, inv.items.Count, "item Count has a wrong value!");
            Assert.AreEqual(testItemEntry, inv.items[0], "item entries are not the same");
            Assert.AreEqual(data.items[0], inv.items[0], "item entries are not the same");

        }
    }
}
