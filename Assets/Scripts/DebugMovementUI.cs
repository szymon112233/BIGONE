﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugMovementUI : MonoBehaviour
{
    public CharacterController charController;

    public Text GroundedText;

    private void Update()
    {
        GroundedText.text = string.Format("Grounded: {0}", charController.isGrounded);
    }
}
