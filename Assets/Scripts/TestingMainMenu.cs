﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestingMainMenu : MonoBehaviour
{
    public const string MainSceneName = "Movement";
    public const string InventorySceneName = "InventoryTesting";
    public const string MapSceneName = "MapGenerator";


    void OpenScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    public void OpenMainScene()
    {
        OpenScene(MainSceneName);
    }

    public void OpenInventoryScene()
    {
        OpenScene(InventorySceneName);
    }

    public void OpenMapScene()
    {
        OpenScene(MapSceneName);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
