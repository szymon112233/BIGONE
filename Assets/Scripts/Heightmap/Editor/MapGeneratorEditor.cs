﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


#if UNITY_EDITOR

[CustomEditor(typeof(MapGenerator))]
public class MapGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        MapGenerator mapGen = (MapGenerator)target;
        if (DrawDefaultInspector())
        {
            if (mapGen.autoUpdate)
            {
                mapGen.DrawMapInEditor();
            }
        }
        
        if (GUILayout.Button("Generate"))
        {
            mapGen.DrawMapInEditor();
        }

        if (GUILayout.Button("Generate from texture"))
        {
            mapGen.GenerateMapFromTexture();
        }

    }
}

#endif