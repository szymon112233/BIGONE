﻿using UnityEngine;
using System.Collections;
using UnityEditor;

#if UNITY_EDITOR
[CustomEditor(typeof(InventoryUI))]
public class InventoryUIEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        //EditorGUILayout.HelpBox("This is a help box", MessageType.Info);
        InventoryUI myScript = (InventoryUI)target;
        if (GUILayout.Button("Spawn random item"))
        {
            myScript.AddRandomItem();
        }
        if (GUILayout.Button("Spawn random generic item"))
        {
            myScript.AddCategoryRandomItem<GenericInvItem>();
        }
        if (GUILayout.Button("Spawn random container item"))
        {
            myScript.AddCategoryRandomItem<ContainerInvItem>();
        }
    }
}
#endif