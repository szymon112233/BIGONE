﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[System.Serializable]
public class LayersDictionary : SerializableDictionaryBase<float, VoxelTerrainLayer> { }

[System.Serializable]
public class VoxelTerrainLayer
{
    public AnimationCurve valueMultiplier;
    public AnimationCurve additionalValue;
    public double frequency;
    //min 1
    public double lacunarity;
    [Range(0, 1)]
    public double persistance;
    //min 0
    public int octaves;
    
}


[CreateAssetMenu]
public class LayersData : ScriptableObject
{
    public LayersDictionary layersDict;

}