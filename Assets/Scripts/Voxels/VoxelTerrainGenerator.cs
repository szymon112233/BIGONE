﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LibNoise;
using LibNoise.Generator;

public class VoxelTerrainGenerator : MonoBehaviour
{

    public Vector3Int size;
    public bool useSmoothTerrain;
    public bool useFlatShading;
    public double frequency;
    public double lacunarity;
    public double persistance;
    public int octaves;
    public int seed;
    public QualityMode quality;
    public float isoLevel;
    public Material terrainMaterial;

    public LayersData currentLayerSetup;

    public bool autoUpdate;

    GameObject terrainRoot;
    List<Vector3> vertices = new List<Vector3>();
    List<int> triangles = new List<int>();

    void Start()
    {
        //Generate();
    }

    public Mesh GenerateChunkMesh(Vector3Int pos, Vector3Int size, int lodsNumber = 1)
    {
        vertices = new List<Vector3>();
        triangles = new List<int>();


        double[,,] density = GetDensity(size + new Vector3Int(1, 1, 1), pos * size);

        for (int x = 0; x < size.x; x++)
        {
            for (int y = 0; y < size.y; y++)
            {
                for (int z = 0; z < size.z; z++)
                {
                    double[] cubeCornersValues = new double[8];
                    cubeCornersValues[0] = density[x, y, z];
                    cubeCornersValues[1] = density[x + 1, y, z];
                    cubeCornersValues[2] = density[x + 1, y + 1, z];
                    cubeCornersValues[3] = density[x, y + 1, z];
                    cubeCornersValues[4] = density[x, y, z + 1];
                    cubeCornersValues[5] = density[x + 1, y, z + 1];
                    cubeCornersValues[6] = density[x + 1, y + 1, z + 1];
                    cubeCornersValues[7] = density[x, y + 1, z + 1];

                    MarchCube(new Vector3(x, y, z), cubeCornersValues);
                }
            }
        }

        Mesh mesh = new Mesh();
        mesh.SetVertices(vertices);
        mesh.SetTriangles(triangles.ToArray(), 0);
        mesh.RecalculateNormals();
        return mesh;
    }

    public void Generate()
    {
        if (terrainRoot != null)
            DestroyImmediate(terrainRoot);
        terrainRoot = new GameObject("Root");
        MeshFilter meshFilter = terrainRoot.AddComponent<MeshFilter>();
        MeshRenderer meshRenderer = terrainRoot.AddComponent<MeshRenderer>();
        meshRenderer.sharedMaterial = terrainMaterial;

        //meshFilter.mesh =  MarchCube(new Vector3(), Random.Range(0, 256));

        vertices = new List<Vector3>();
        triangles = new List<int>();


        double[,,] density = GetDensity(size + new Vector3Int(1, 1, 1));

        for (int x = 0; x < size.x; x++)
        {
            for (int y = 0; y < size.y; y++)
            {
                for (int z = 0; z < size.z; z++)
                {
                    double[] cubeCornersValues = new double[8];
                    cubeCornersValues[0] = density[x, y, z];
                    cubeCornersValues[1] = density[x + 1, y, z];
                    cubeCornersValues[2] = density[x + 1, y +1, z];
                    cubeCornersValues[3] = density[x, y + 1, z];
                    cubeCornersValues[4] = density[x, y, z + 1];
                    cubeCornersValues[5] = density[x + 1, y, z + 1];
                    cubeCornersValues[6] = density[x + 1, y + 1, z + 1];
                    cubeCornersValues[7] = density[x, y + 1, z + 1];

                    MarchCube(new Vector3(x, y, z), cubeCornersValues);
                }
            }
        }

        Mesh mesh = new Mesh();
        mesh.SetVertices(vertices);
        mesh.SetTriangles(triangles.ToArray(), 0);
        mesh.RecalculateNormals();
        //mesh.RecalculateTangents();
        meshFilter.mesh = mesh;
    }

    double[,,] GetDensity(Vector3Int size, Vector3 pos = new Vector3())
    {
        Perlin perlinNoise = new Perlin(frequency, lacunarity, persistance, octaves, seed, QualityMode.Low);

        double[,,] density = new double[size.x, size.y, size.z];

        for (int x = 0; x < size.x; x++)
        {
            for (int y = 0; y < size.y; y++)
            {
                for (int z = 0; z < size.z; z++)
                {
                    VoxelTerrainLayer currentLayer = GetLayerFromHeight(pos.y + y);
                    density[x, y, z] = NormalizeNoiseValue(perlinNoise.GetValue(pos.x + x,pos.y + y, pos.z + z)) * currentLayer.valueMultiplier.Evaluate((float)y/ size.y) + currentLayer.additionalValue.Evaluate((float)y / size.y);
                    //Debug.Log(currentLayer.valueMultiplier.Evaluate((float)y/ (float)size.y));
                    //Debug.Log((float)y / (float)size.y);

                    //if (y == 0 || y == size.y - 1)
                    //    density[x, y, z] = 1.0f;
                    //density[x, y, z] = Random.Range(-1.0f, 1.0f);
                    //Debug.LogFormat("Perlin Noise value at [{0}|{1}|{2}] is {3}", pos.x + x, pos.y + y, pos.z + z, density[x, y, z]);
                }
            }
        }

        return density;
    }

    void MarchCube(Vector3 pos, double[] cubeCornersValues)
    {
        int configIndex = GetConfigurationIndex(cubeCornersValues);

        if (configIndex < 0 || configIndex > 255)
            return;

        if (configIndex == 0 || configIndex == 255)
            return;

        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                int index = MarchingCubesConsts.triTable[configIndex, i * 3 + j];

                if (index == -1)
                    return;

                Vector3 vert1 = MarchingCubesConsts.CornerTable[MarchingCubesConsts.EdgeIndexes[index, 0]];
                Vector3 vert2 = MarchingCubesConsts.CornerTable[MarchingCubesConsts.EdgeIndexes[index, 1]];

                Vector3 vertexPosition;
                if (useSmoothTerrain)
                {
                    double vert1Value = cubeCornersValues[MarchingCubesConsts.EdgeIndexes[index, 0]];
                    double vert2Value = cubeCornersValues[MarchingCubesConsts.EdgeIndexes[index, 1]];

                    double differnce = vert2Value - vert1Value;

                    if (differnce == 0)
                        differnce = isoLevel;
                    else
                        differnce = (isoLevel - vert1Value) / differnce;


                    vertexPosition = pos + vert1 + ((vert2 - vert1) * (float)differnce);
                }
                else
                    vertexPosition = pos + (vert1 + vert2) / 2.0f;

                if (useFlatShading)
                {
                    vertices.Add(vertexPosition);
                    triangles.Add(vertices.Count - 1);
                }
                else
                {
                    int foundIndex = VertForIndex(vertexPosition);
                    if (foundIndex == -1)
                    {
                        vertices.Add(vertexPosition);
                        triangles.Add(vertices.Count - 1);
                    }
                    else
                    {
                        triangles.Add(foundIndex);
                    }
                }
            }
        }
    }

    int VertForIndex(Vector3 vert)
    {
        for (int i = 0; i < vertices.Count; i++)
        {
            if (vertices[i] == vert)
                return i;
        }
        return -1;
    }

    int GetConfigurationIndex(double[] cubeCornersValues)
    {
        int configIndex = 0;
        if (cubeCornersValues[0] > isoLevel) configIndex |= 1;
        if (cubeCornersValues[1] > isoLevel) configIndex |= 2;
        if (cubeCornersValues[2] > isoLevel) configIndex |= 4;
        if (cubeCornersValues[3] > isoLevel) configIndex |= 8;
        if (cubeCornersValues[4] > isoLevel) configIndex |= 16;
        if (cubeCornersValues[5] > isoLevel) configIndex |= 32;
        if (cubeCornersValues[6] > isoLevel) configIndex |= 64;
        if (cubeCornersValues[7] > isoLevel) configIndex |= 128;

        return configIndex;
    }

    VoxelTerrainLayer GetLayerFromHeight(float yLevel)
    {
        VoxelTerrainLayer currentLayer = new VoxelTerrainLayer();
        int layerIndex = 0;
        foreach (var item in currentLayerSetup.layersDict)
        {
            if (yLevel < item.Key)
                break;
            currentLayer = item.Value;
            //Debug.Log(layerIndex);
            layerIndex++;
        }

        return currentLayer;
    }

    double NormalizeNoiseValue(double value)
    {
        double max = 1.0;
        double min = -1.0;
        if (value < min)
            return 0.0;
        if (value > max)
            return 1.0;

        return ((value - (min)) / (max - (min)));
    }
}
