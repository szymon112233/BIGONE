﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VChunk 
{
    public static Vector3Int chunkSize = new Vector3Int(32, 32, 32);
    public VoxelTerrainGenerator generator;
    public Vector3Int position;
    public bool isMeshGenerated = false;

    GameObject meshObejct;
    Bounds bounds;

    MeshRenderer meshRenderer;
    MeshFilter meshFilter;
    MeshCollider meshCollider;

    public VChunk(Vector3Int position,  VoxelTerrainGenerator generator, Transform parent = null)
    {
        this.position = position;
        this.generator = generator;
        meshObejct = new GameObject("Terrain Chunk");
        meshObejct.transform.SetParent(parent);
        meshObejct.transform.localPosition = position * chunkSize;
        meshRenderer = meshObejct.AddComponent<MeshRenderer>();
        meshRenderer.material = generator.terrainMaterial;
        meshFilter = meshObejct.AddComponent<MeshFilter>();
        meshCollider = meshObejct.AddComponent<MeshCollider>();
        GenerateMeshes();
    }

    public void GenerateFilledChunk()
    {
        for (int z = 0; z < chunkSize.z; z++)
        {
            for (int y = 0; y < chunkSize.y; y++)
            {
                for (int x = 0; x < chunkSize.x; x++)
                {
                    Debug.Log(z * chunkSize.y + y * chunkSize.x + x);
                }
            }
        }
    }

    public void GenerateMeshes()
    {
        meshFilter.mesh = generator.GenerateChunkMesh(position, chunkSize);
    }

    public void DestroyChunk()
    {
        GameObject.DestroyImmediate(meshObejct);
    }
}
