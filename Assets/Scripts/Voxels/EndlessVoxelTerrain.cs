﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EndlessVoxelTerrain : MonoBehaviour
{
    public Vector3Int size;
    public VoxelTerrainGenerator generator;
    Dictionary<Vector3Int, VChunk> chunkDictionary;


    private void Awake()
    {
        chunkDictionary = new Dictionary<Vector3Int, VChunk>();
    }

    void Start()
    {
        GenerateWholeWorld();
    }

    public void GenerateWholeWorld()
    {
        ClearChunkMesh();

        float timeStart = Time.realtimeSinceStartup;
        for (int x = 0; x < size.x; x++)
        {
            for (int y = 0; y < size.y; y++)
            {
                for (int z = 0; z < size.z; z++)
                {
                    Vector3Int pos = new Vector3Int(x, y, z);
                    chunkDictionary.Add(pos, new VChunk(pos, generator, transform));
                }
            }
        }
        float timeElapsed = Time.realtimeSinceStartup - timeStart;
        Debug.LogFormat("Generated whole terrain in: {0} seconds.", timeElapsed);
    }

    private void ClearChunkMesh()
    {
        if (chunkDictionary == null)
        {
            chunkDictionary = new Dictionary<Vector3Int, VChunk>();
            return;
        }
            

        foreach (VChunk chunk in chunkDictionary.Values)
        {
            chunk.DestroyChunk();
        }
        chunkDictionary.Clear();
    }
}
