﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


#if UNITY_EDITOR

[CustomEditor(typeof(EndlessVoxelTerrain))]
public class EndlessVoxelTerrainEditor : Editor
{
    public override void OnInspectorGUI()
    {
        EndlessVoxelTerrain terrain = (EndlessVoxelTerrain)target;

        DrawDefaultInspector();

        if (GUILayout.Button("Generate Preview Mesh"))
        {
            terrain.GenerateWholeWorld();
        }


    }
}

#endif