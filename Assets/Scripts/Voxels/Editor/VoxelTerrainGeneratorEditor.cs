﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


#if UNITY_EDITOR

[CustomEditor(typeof(VoxelTerrainGenerator))]
public class VoxelTerrainGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        VoxelTerrainGenerator generator = (VoxelTerrainGenerator)target;
        if (DrawDefaultInspector())
        {
            if (generator.autoUpdate)
            {
                generator.Generate();
            }
        }

        if (GUILayout.Button("Generate"))
        {
            generator.Generate();
        }


    }
}

#endif