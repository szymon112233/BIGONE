﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;



public class InventoryUI : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    
    //inventory metadata
    public int myInvID;
    public Inventory cachedInventory;
    public bool initOnStart;

    [Header("References")]
    //Inventory UI data & references
    public GameObject itemGraphicsPrefab;
    public RectTransform graphicsParent;
    public RectTransform borderRectTransform;
    public RectTransform backgroundRectTransform;
    public RectTransform gridRectTransform;
    public RectTransform nameTransform;
    public GameObject placeHelper;
    public GameObject closeButton;
    public Text nameText;
    private bool isCursorOver;

    private void Awake()
    {
        borderRectTransform.GetComponent<DragUtilityWrapper>().OnDragEvent += onDrag;
    }

    private void Start()
    {
        if (initOnStart)
        {
            InventorySerializableData data = cachedInventory.SerializeInventory();
            cachedInventory.Init(cachedInventory.inventorySize, cachedInventory.closable, cachedInventory.movable);
            cachedInventory.SetInventory(data);
            if (myInvID >= 0)
            {
                if (!Globals.Instance.inventorySystem.DoesInventoryExist(myInvID))
                {
                    Globals.Instance.inventorySystem.SetInventory(myInvID, cachedInventory);
                }

            }
            else
            {
                myInvID = Globals.Instance.inventorySystem.AddNewInventory(cachedInventory);
            }
            Init(myInvID, graphicsParent.anchoredPosition);
            
        }
    }

    private void OnDestroy()
    {
        borderRectTransform.GetComponent<DragUtilityWrapper>().OnDragEvent -= onDrag;
    }

    public void Init(int invID, Vector2 position, ItemInvEntry creator = null)
    {
        Debug.LogFormat("Attempting to init InventoryUI with invID = {0}, on {1}", invID, position);
        if (!Globals.Instance.inventorySystem.GetInventory(invID, out cachedInventory))
        {
            Debug.LogWarningFormat("Could not find inventory with ID {0}", invID);
            return;
        }
        Vector2Int size = cachedInventory.inventorySize;

        //if (creator != null)
        //    myOwningContainerItem = creator;
        closeButton.SetActive(cachedInventory.closable);
        Vector2 nameSize = new Vector2(size.x * 75 /2, 75/2);
        nameTransform.sizeDelta = nameSize;
        nameTransform.anchoredPosition = nameSize / 2;
        nameText.text = cachedInventory.name;
        graphicsParent.sizeDelta = new Vector2(size.x * 75, size.y * 75);
        borderRectTransform.sizeDelta = new Vector2(size.x * 75 * 1.122f, size.y * 75 * 1.122f);
        backgroundRectTransform.sizeDelta = new Vector2(size.x * 75, size.y * 75);
        gridRectTransform.sizeDelta = new Vector2(size.x * 75, size.y * 75);
        gridRectTransform.GetComponent<RawImage>().uvRect = new Rect(Vector2.zero, new Vector2(size.x, size.y));

        if (position != Vector2.zero)
            graphicsParent.anchoredPosition = position;

        foreach (ItemInvEntry item in cachedInventory.items)
        {
            CreateUIItem(item);
        }
    }

    public void SetPositionPixelCords(Vector3 pos)
    {
        graphicsParent.position = pos;
    }

    public bool AddItem(ItemInvEntry itemEntry)
    {
        RectTransform itemRectTransform = itemEntry.UIgameObject.GetComponent<RectTransform>();
        itemRectTransform.SetParent(graphicsParent);
        itemRectTransform.anchoredPosition = new Vector2(itemEntry.position.x * 75, itemEntry.position.y * -75);
        itemEntry.UIgameObject.GetComponent<InvItemUIButton>().OnPickup += PickupItem;
        itemEntry.UIgameObject.GetComponent<InvItemUIButton>().currentInv = this;
        return true;
    }

    void PickupItem(ItemInvEntry item)
    {
        if (cachedInventory.RemoveItem(item))
        {
            item.UIgameObject.GetComponent<InvItemUIButton>().currentInv = null;
            item.UIgameObject.GetComponent<InvItemUIButton>().prevInv = this;
            item.UIgameObject.GetComponent<InvItemUIButton>().OnPickup -= PickupItem;
        }
    }

    public bool DropItem(ItemInvEntry item)
    {
        if (cachedInventory.AddItem(item))
        {
            return AddItem(item);
        }
        else
        {
            return false;
        }
    }

    bool CreateUIItem(ItemInvEntry item)
    {
        item.UIgameObject = Instantiate(itemGraphicsPrefab, graphicsParent);
        item.UIgameObject.GetComponent<InvItemUIButton>().Init(item);
        if (AddItem(item))
        {
            return true;
        }
        else
        {
            Destroy(item.UIgameObject);
            return false;
        }
    }

    public void AddRandomItem()
    {
        List<GenericInvItem> items = new List<GenericInvItem>(Globals.Instance.itemDatabase.GetAllItems().Values);
        GenericInvItem itemData = items[Random.Range(0, items.Count)];
        ItemInvEntry item = new ItemInvEntry();
        item.itemData = itemData;
        item.currentInvSize = itemData.size;
        item.position = new Vector2Int(Random.Range(0, cachedInventory.inventorySize.x), Random.Range(0, cachedInventory.inventorySize.y));
        item.count = Random.Range(1, 999);

        if (!cachedInventory.AddItem(item))
        {
            Destroy(item.UIgameObject);
            return;
        }
        if (!CreateUIItem(item))
            Destroy(item.UIgameObject);
    }

    public void AddCategoryRandomItem<T>() where T : GenericInvItem
    {
        ItemInvEntry item = new ItemInvEntry();
        List<T> items = new List<T>(Globals.Instance.itemDatabase.GetItemsOfType<T>().Values);
        if (items.Count == 0)
            return;
        item.itemData = items[Random.Range(0, items.Count)];

        item.currentInvSize = item.itemData.size;
        item.position = new Vector2Int(Random.Range(0, cachedInventory.inventorySize.x), Random.Range(0, cachedInventory.inventorySize.y));
        item.count = Random.Range(1, 999);

        if (!cachedInventory.AddItem(item))
        {
            Destroy(item.UIgameObject);
            return;
        }
        if (!CreateUIItem(item))
                Destroy(item.UIgameObject);
    }

    public void CloseInventoryWindow()
    {
        Destroy(gameObject);
    }

    //TODO: I don't like these calculation here... I have to figure out a better way to make them
    public void OnDrop(PointerEventData eventData)
    {
        if (!eventData.pointerDrag.GetComponent<InvItemUIButton>())
            return;

        Vector2Int desiredSlot = ScreenPointToSlot(eventData.position);
        Debug.LogFormat("On Drop: {0}, {1}, {2}", eventData.pointerDrag, eventData.position, desiredSlot);

        ItemInvEntry itemEntry;
        if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Globals.Instance.inventorySystem.currentlyDraggedItem.count > 1)
        {
            if (!cachedInventory.isSpaceFree(desiredSlot, Globals.Instance.inventorySystem.currentlyDraggedItem.currentInvSize))
            {
                eventData.pointerDrag.GetComponent<InvItemUIButton>().ReturnToPrevInv();
                return;
            }

            bool isEqualSplit = eventData.pointerDrag.GetComponent<InvItemUIButton>().myEntry.count % 2 == 0;
            eventData.pointerDrag.GetComponent<InvItemUIButton>().myEntry.count /= 2;
            eventData.pointerDrag.GetComponent<InvItemUIButton>().UpdateUI();
            eventData.pointerDrag.GetComponent<InvItemUIButton>().ReturnToPrevInv();

            itemEntry = new ItemInvEntry(eventData.pointerDrag.GetComponent<InvItemUIButton>().myEntry);
            if (!isEqualSplit)
                itemEntry.count++;
            GameObject go = Instantiate(itemGraphicsPrefab, graphicsParent);
            go.GetComponent<InvItemUIButton>().Init(itemEntry);
            
        }
        else
        {
            itemEntry = eventData.pointerDrag.GetComponent<InvItemUIButton>().myEntry;
        }

        Vector2Int lastPos = itemEntry.position;
        itemEntry.position = desiredSlot;
        if (!DropItem(itemEntry))
        {
            if (cachedInventory.StackItem(itemEntry) == 0)
            {
                Destroy(itemEntry.UIgameObject);
            }
            else
            {
                itemEntry.position = lastPos;
                eventData.pointerDrag.GetComponent<InvItemUIButton>().ReturnToPrevInv();
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isCursorOver = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isCursorOver = false;
    }

    //TODO: cache the calculated spot so that we don't have to recalculate the rest if the spot is the same
    private void Update()
    {
        if (isCursorOver && Globals.Instance.inventorySystem.currentlyDraggedItem != null && EventSystem.current.IsPointerOverGameObject())
        {
            //Debug.LogFormat("Yup, currentlyDraggedItem: {0}", currentlyDraggedItem.UIgameObject.name);
            Vector2 localPoint;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(graphicsParent, Input.mousePosition, null, out localPoint);
            Vector2Int desiredSlot = new Vector2Int((int)localPoint.x / 75, -(int)localPoint.y / 75);
            placeHelper.GetComponent<RectTransform>().sizeDelta = Globals.Instance.inventorySystem.currentlyDraggedItem.currentInvSize * 75;
            placeHelper.GetComponent<RectTransform>().anchoredPosition = new Vector2(desiredSlot.x * 75, desiredSlot.y * -75);
            if (cachedInventory.isSpaceFree(desiredSlot, Globals.Instance.inventorySystem.currentlyDraggedItem.currentInvSize))
            {
                if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Globals.Instance.inventorySystem.currentlyDraggedItem.count > 1)
                       placeHelper.GetComponent<Image>().color = Color.yellow;
                else
                    placeHelper.GetComponent<Image>().color = Color.green;
            }
            else
            {
                ItemInvEntry item;
                if (Globals.Instance.inventorySystem.currentlyDraggedItem.itemData.Stackable && cachedInventory.GetItemAt(desiredSlot, out item))
                {
                    if (item.itemData == Globals.Instance.inventorySystem.currentlyDraggedItem.itemData)
                    {
                        if (item.count + Globals.Instance.inventorySystem.currentlyDraggedItem.count <= item.itemData.maxStackSize)
                        {
                            placeHelper.GetComponent<Image>().color = Color.green;
                        }
                        else
                        {
                            placeHelper.GetComponent<Image>().color = Color.red;
                        }
                    }
                    else
                    {
                        placeHelper.GetComponent<Image>().color = Color.red;
                    }
                }
                else
                {
                    placeHelper.GetComponent<Image>().color = Color.red;
                }
            }
                
        }
        placeHelper.SetActive(isCursorOver && Globals.Instance.inventorySystem.currentlyDraggedItem != null);
    }

    private void onDrag(PointerEventData eventData)
    {
        if (cachedInventory != null &&  cachedInventory.movable)
            graphicsParent.position = Input.mousePosition;
    }

    Vector2Int ScreenPointToSlot(Vector2 screenPoint)
    {
        Vector2 localPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(graphicsParent, screenPoint, null, out localPoint);
        Vector2Int desiredSlot = new Vector2Int((int)localPoint.x / 75, -(int)localPoint.y / 75);
        return desiredSlot;
    }
}
