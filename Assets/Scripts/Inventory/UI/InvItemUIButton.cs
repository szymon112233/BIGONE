﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InvItemUIButton : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler, IPointerClickHandler
{
    public ItemInvEntry myEntry;
    public InventoryUI currentInv;
    public InventoryUI prevInv;

    public RectTransform rectTransform;
    public CanvasGroup canvasGroup;
    public Image graphics;
    public TMPro.TextMeshProUGUI countText;
    public GameObject myInventoryGO;

    private Canvas dragCanvas;

    public System.Action<ItemInvEntry> OnPickup;
    public System.Action<ItemInvEntry> OnDrop;

    bool isDragged;
    private float lastClickTime;

    private void Awake()
    {
        dragCanvas = Globals.Instance.inventorySystem.dragCanvas;
    }

    private void OnDestroy()
    {
        //TODO: hacky, please fix
        if (Globals.Instance != null && Globals.Instance.inventorySystem.currentlyDraggedItem == myEntry)
            Globals.Instance.inventorySystem.currentlyDraggedItem = null;

    }

    public void Init(ItemInvEntry entry)
    {
        if (entry == null)
        {
            Debug.LogWarningFormat("Cannot init {0}, item entry provided is a null", gameObject.name);
            return;
        }
        myEntry = entry;
        myEntry.UIgameObject = gameObject;
        UpdateUI();
    }

    //TODO: make it return to previous rotation, make all the data available in this class so that it works called anywhere
    public void ReturnToPrevInv()
    {
        if (prevInv)
            prevInv.DropItem(myEntry);
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Globals.Instance.inventorySystem.currentlyDraggedItem = null;
        canvasGroup.blocksRaycasts = true;
        isDragged = false;
        if (currentInv == null)
            ReturnToPrevInv();
        OnDrop?.Invoke(myEntry);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        StartDrag();
    }

    void StartDrag()
    {
        Globals.Instance.inventorySystem.currentlyDraggedItem = myEntry;
        canvasGroup.blocksRaycasts = false;
        if (dragCanvas)
            rectTransform.SetParent(dragCanvas.transform);
        isDragged = true;
        if (OnPickup != null)
            OnPickup(myEntry);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        float clickTimeDelta = eventData.clickTime - lastClickTime;
        lastClickTime = eventData.clickTime;

        //Debug.LogFormat("clickTimeDelta: {0}, clickCount: {1}", clickTimeDelta, eventData.clickCount);
        //TODO: make a better way to tell the button
        if (clickTimeDelta < 0.55f)
        {
            OnDoubleClick(eventData.button);
        }
        else
        {
            OnSingleClick(eventData.button);
        }
    }

    void Update()
    {
        if (isDragged && Input.GetKeyDown(KeyCode.R))
            Rotate();
    }

    public void UpdateUI()
    {
        gameObject.name = string.Format("{0}x{1}", myEntry.itemData.name, myEntry.count);
        graphics.sprite = myEntry.itemData.sprite;
        if (myEntry.itemData.Stackable)
            countText.SetText(myEntry.count.ToString());
        else
            countText.gameObject.SetActive(false);
        rectTransform.sizeDelta = new Vector2(myEntry.currentInvSize.x * 75, myEntry.currentInvSize.y * 75);
        graphics.rectTransform.sizeDelta = rectTransform.sizeDelta;
    }

    void OnSingleClick(PointerEventData.InputButton button)
    {
        Debug.LogFormat("Single click! Button: {0}", button);
    }

    void OnDoubleClick(PointerEventData.InputButton button)
    {
        Debug.Log("Double Click");
        //big no no - has to be a better way to make it work only for Container Items
        if (myEntry.itemData.GetType() == typeof(ContainerInvItem))
        {
            OpenMyInventory();
        }
    }

    void Rotate()
    {
        //Flip the current Size
        myEntry.currentInvSize = new Vector2Int(myEntry.currentInvSize.y, myEntry.currentInvSize.x);
        //set the transform size for the new current size
        rectTransform.sizeDelta = new Vector2(myEntry.currentInvSize.x * 75, myEntry.currentInvSize.y * 75);
        //if we have original rotation (2,4)
        if (myEntry.currentInvSize == myEntry.itemData.size)
        {
            graphics.rectTransform.localRotation = Quaternion.AngleAxis(0, Vector3.forward);
            graphics.rectTransform.anchoredPosition = new Vector2(0, 0);
        }
        //(1,3)
        else
        {
            //if horizontal (1)
            if (myEntry.itemData.size.x > myEntry.itemData.size.y)
            {
                graphics.rectTransform.localRotation = Quaternion.AngleAxis(-90, Vector3.forward);
                graphics.rectTransform.anchoredPosition = new Vector2(75 * myEntry.itemData.size.y, 0);
            }
            // (3)
            else if (myEntry.itemData.size.x < myEntry.itemData.size.y)
            {
                graphics.rectTransform.localRotation = Quaternion.AngleAxis(90, Vector3.forward);
                graphics.rectTransform.anchoredPosition = new Vector2(0, -75 * myEntry.itemData.size.x);
            }
        }
    }

    void OpenMyInventory()
    {
        if (myInventoryGO == null)
        {
            myInventoryGO = Instantiate(Globals.Instance.InventoryTemplatePrefab);
            ContainerInvItem containerInfo = (ContainerInvItem)myEntry.itemData;
            InventoryUI invUI = myInventoryGO.GetComponent<InventoryUI>();
            Inventory inv;
            if (!Globals.Instance.inventorySystem.GetInventory(myEntry.myInvKey, out inv))
            {
                inv.Init(containerInfo.myInvSize, true, true);
                myEntry.myInvKey = Globals.Instance.inventorySystem.AddNewInventory(inv);
            }
            invUI.Init(myEntry.myInvKey, Input.mousePosition, myEntry);
            //TODO: Dirty fix bc, i don;t want to figure it out now, bigger stuff
            invUI.SetPositionPixelCords(Input.mousePosition);
        }
    }
}
