﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemInvEntry
{
    public GenericInvItem itemData = null;
    public int count = 1;
    public Vector2Int position = new Vector2Int();
    public Vector2Int currentInvSize = new Vector2Int();
    public int myInvKey = -1;

    public GameObject UIgameObject;

    public ItemInvEntry()
    {
    }

    public ItemInvEntry(GenericInvItem itemData, int count, Vector2Int position, Vector2Int currentInvSize, int myInvKey, GameObject uIgameObject = null)
    {
        this.itemData = itemData;
        this.count = count;
        this.position = position;
        this.currentInvSize = currentInvSize;
        this.myInvKey = myInvKey;
        UIgameObject = uIgameObject;
    }

    public ItemInvEntry(ItemInvEntry copyFrom)
    {
        this.itemData = copyFrom.itemData;
        this.count = copyFrom.count;
        this.position = copyFrom.position;
        this.currentInvSize = copyFrom.currentInvSize;
        this.myInvKey = copyFrom.myInvKey;
    }

}

[System.Serializable]
public class InventorySerializableData
{
    public Vector2Int size;
    public List<ItemInvEntry> items;
}

[System.Serializable]
public class Inventory
{
    public string name;
    public List<ItemInvEntry> items;
    public Vector2Int inventorySize;
    public bool closable;
    public bool movable;
    private bool[,] slotsOccupancy;

    public bool Init(Vector2Int size, bool closable = false, bool movable = false)
    {
        if (size == null  || size.x < 1 || size.y <1)
        {
            Debug.LogWarning("Inventory, Init: Invalid size! x & y must be greater than 0!");
            return false;
        }
        if (size.x > 256 || size.y > 256)
        {
            Debug.LogWarning("Inventory, Init: Invalid size! x & y can be 256 at max!");
            return false;
        }
        inventorySize = size;
        this.closable = closable;
        this.movable = movable;
        ClearItems();
        return true;
    }

    private void ClearItems()
    {
        items = new List<ItemInvEntry>();
        slotsOccupancy = new bool[inventorySize.x, inventorySize.y];
    }

    public bool SetInventory(InventorySerializableData data)
    {
        if (data == null)
            return false;
        inventorySize = data.size;
        ClearItems();
        foreach (ItemInvEntry item in data.items)
        {
            if (!AddItem(item))
                return false;
        }
        return true;
    }

    public InventorySerializableData SerializeInventory()
    {
        InventorySerializableData data = new InventorySerializableData();
        data.items = items;
        data.size = inventorySize;

        return data;
    }

    public bool isSpaceFree(Vector2Int position, Vector2Int size)
    {
        for (int x = position.x; x < position.x + size.x; x++)
        {
            for (int y = position.y; y < position.y + size.y; y++)
            {
                //Debug.LogFormat("isSpaceFree:({0},{1})", x, y);
                if (x > inventorySize.x - 1 || y > inventorySize.y - 1)
                    return false;
                if (slotsOccupancy[x, y])
                    return false;
            }
        }
        return true;
    }

    void MarkSlotsOccupied(Vector2Int position, Vector2Int size, bool occupied = true)
    {
        for (int x = position.x; x < position.x + size.x; x++)
        {
            for (int y = position.y; y < position.y + size.y; y++)
            {
                slotsOccupancy[x, y] = occupied;
            }
        }
    }

    public bool AddItem(ItemInvEntry itemEntry)
    {
        Debug.LogFormat("Trying to add a new item to inventory at:({0},{1}), size:({2},{3})",
            itemEntry.position.x,
            itemEntry.position.y,
            itemEntry.currentInvSize.x,
            itemEntry.currentInvSize.y);

        if (isSpaceFree(itemEntry.position, itemEntry.currentInvSize))
        {
            MarkSlotsOccupied(itemEntry.position, itemEntry.currentInvSize);
            items.Add(itemEntry);
            return true;
        }
        return false;
    }

    public int StackItem(ItemInvEntry itemEntry)
    {
        Debug.LogFormat("Trying to stack items at:({0},{1}), size:({2},{3})",
            itemEntry.position.x,
            itemEntry.position.y,
            itemEntry.currentInvSize.x,
            itemEntry.currentInvSize.y);

        if (!itemEntry.itemData.Stackable)
        {
            Debug.LogWarningFormat("Cannot stack item: {0}, since it's nto stackable.", itemEntry);
            return -1;
        }

        ItemInvEntry foundItemEntry;
        if (GetItemAt(itemEntry.position, out foundItemEntry))
        {
            if (foundItemEntry.itemData == itemEntry.itemData)
            {
                if (foundItemEntry.count + itemEntry.count <= itemEntry.itemData.maxStackSize)
                {
                    foundItemEntry.count += itemEntry.count;
                    //TODO: make it so that count update sends out an event and we don't have to update this here
                    foundItemEntry.UIgameObject.GetComponent<InvItemUIButton>().UpdateUI();
                    return 0;
                }
                else
                {
                    foundItemEntry.count += itemEntry.count;
                    int itemsLeft = foundItemEntry.count - foundItemEntry.itemData.maxStackSize;
                    foundItemEntry.count = foundItemEntry.itemData.maxStackSize;
                    itemEntry.count = itemsLeft;
                    //TODO: make it so that count update sends out an event and we don't have to update this here
                    foundItemEntry.UIgameObject.GetComponent<InvItemUIButton>().UpdateUI();
                    itemEntry.UIgameObject.GetComponent<InvItemUIButton>().UpdateUI();
                    return itemsLeft;
                }
            }
        }
        return -1;
    }

    public bool RemoveItem(ItemInvEntry item)
    {
        if (items.Contains(item))
        {
            items.Remove(item);
            MarkSlotsOccupied(item.position, item.currentInvSize, false);
            return true;
        }
        return false;
    }

    public bool GetItemAt(Vector2Int slot, out ItemInvEntry item)
    {
        item = new ItemInvEntry();

        if (isSpaceFree(slot, new Vector2Int(1, 1)))
            return false;
        foreach (ItemInvEntry currentItem in items)
        {
            Vector2Int size = currentItem.currentInvSize;
            Vector2Int position = currentItem.position;
            for (int x = position.x; x < position.x + size.x; x++)
            {
                for (int y = position.y; y < position.y + size.y; y++)
                {
                    if (slot.x == x && slot.y == y)
                    {
                        item = currentItem;
                        return true;
                    }
                }
            }
        }

        return false;
    }
}

