﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GenericInvItem", menuName = "Items/GenericInvItem")]
public class GenericInvItem : ScriptableObject
{
    public string itemName;
    public Sprite sprite;
    public Vector2Int size = new Vector2Int(1, 1);
    [field: SerializeField]
    public virtual bool Stackable { get; set; }
    [field: SerializeField]
    public virtual int maxStackSize { get; set; }
}