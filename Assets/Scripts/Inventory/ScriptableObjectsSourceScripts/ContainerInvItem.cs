﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ContainerInvItem", menuName = "Items/ContainerInvItem")]
public class ContainerInvItem : GenericInvItem
{
    public override bool Stackable
    {
        get
        {
            return false;
        }
        set
        {

        }
    }

    public override int maxStackSize
    {
        get
        {
            return 1;
        }
        set
        {

        }
    }

    public Vector2Int myInvSize;
}
