﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;

[System.Serializable]
public class ItemDatabaseDictionary : SerializableDictionaryBase<long, GenericInvItem> { }


[CreateAssetMenu]
public class ItemDatabase : ScriptableObject
{
    [SerializeField]
    private ItemDatabaseDictionary items;

    public Dictionary<long, T> GetItemsOfType<T>() where T : GenericInvItem
    {
        Dictionary<long, T> foundItems = new Dictionary<long, T>();

        foreach (long key in items.Keys)
        {
            if (items[key].GetType() == typeof(T))
            {
                T item = (T)items[key];
                foundItems.Add(key, item);
            }
        }
        return foundItems;
    }

    public Dictionary<long, GenericInvItem> GetAllItems()
    {
        return items.Clone();
    }

}
