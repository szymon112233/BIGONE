﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySystem : Singleton<InventorySystem>
{
    public ItemInvEntry currentlyDraggedItem;
    public Canvas dragCanvas;

    public GameObject dragCanvasPrefab;

    private void Awake()
    {
        if (dragCanvas == null)
            dragCanvas = GameObject.FindGameObjectWithTag("DragCanvas").GetComponent<Canvas>();
            if (dragCanvas == null)
                dragCanvas = Instantiate(dragCanvasPrefab).GetComponent<Canvas>();
    }

    #region Inventory Storage

    private Dictionary<int, Inventory> savedInventories;

    public InventorySystem()
    {
        savedInventories = new Dictionary<int, Inventory>();
    }

    public bool GetInventory(int key, out Inventory inv)
    {
        if (savedInventories.ContainsKey(key))
        {
            inv = savedInventories[key];
            return true;
        }

        inv = new Inventory();
        return false;
    }

    public bool DoesInventoryExist(int key)
    {
        return savedInventories.ContainsKey(key);
    }

    public int AddNewInventory(Inventory inv)
    {
        int newKey = Random.Range(0, int.MaxValue);
        //TODO: Make sure we don't have an infinite loop here...
        while (savedInventories.ContainsKey(newKey))
        {
            newKey = Random.Range(0, int.MaxValue);
        }
        savedInventories.Add(newKey, inv);

        return newKey;
    }

    public int CreateNewInventory(int newKey = -1)
    {
        if (newKey == -1)
        {
            newKey = Random.Range(0, int.MaxValue);
            //TODO: Make sure we don't have an infinite loop here...
            while (savedInventories.ContainsKey(newKey))
            {
                newKey = Random.Range(0, int.MaxValue);
            }
        }
        else
        {
            if (savedInventories.ContainsKey(newKey))
            {
                Debug.LogWarningFormat("Cannot create new inventory with key: {0}, the ky is already in use.", newKey);
                return -1;
            }
        }
        
        savedInventories.Add(newKey, new Inventory());

        return newKey;
    }

    public bool SetInventory(int key, Inventory inv, bool shouldOverride = false)
    {
        if (savedInventories.ContainsKey(key))
        {
            if (shouldOverride)
            {
                savedInventories[key] = inv;
                return true;
            }
            else
            {
                Debug.LogErrorFormat("Cannot save inventory with key:{0} since it already existst! Set shouldOverride to true to override it.");
                return false;
            }
        }
        else
        {
            savedInventories[key] = inv;
            return true;
        }
    }

#endregion
}
