﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryTestScene : MonoBehaviour
{
    public List<Inventory> inventories;
    public List<int> inventoriesIDs;

    public GameObject InventoryUIPrefab;

    public Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        inventories = new List<Inventory>();
        inventoriesIDs = new List<int>();

        inventoriesIDs.Add(Globals.Instance.inventorySystem.CreateNewInventory());
        Inventory inv;
        Globals.Instance.inventorySystem.GetInventory(inventoriesIDs[0], out inv);
        inv.Init(new Vector2Int(8, 8));
        inventories.Add(inv);

        inventoriesIDs.Add(Globals.Instance.inventorySystem.CreateNewInventory());
        Inventory inv2;
        Globals.Instance.inventorySystem.GetInventory(inventoriesIDs[1], out inv2);
        inv2.Init(new Vector2Int(8, 8), false, true);
        inventories.Add(inv2);

        GameObject go = Instantiate(InventoryUIPrefab);
        InventoryUI invUI = go.GetComponent<InventoryUI>();
        invUI.Init(inventoriesIDs[0], new Vector2(212, -187));

        GameObject go2 = Instantiate(InventoryUIPrefab);
        InventoryUI invUI2 = go2.GetComponent<InventoryUI>();
        invUI2.Init(inventoriesIDs[1], new Vector2(1314, -187));
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //Debug.LogFormat("Mouse Position in Pixel coords: {0}", Input.mousePosition);
            //Debug.LogFormat("Mouse Position in World coords: {0}", cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1)));
            //Debug.LogFormat("Mouse Position in Viewport coords: {0}", cam.ScreenToViewportPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1)));
        }
            
    }
}
